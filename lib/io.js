const readline = require('readline');


exports.print_category_choices = function (choicesArr) {

  for (i = 0; i < choicesArr.length; i++) {
    console.log(`${i+1}. ${choicesArr[i]}`)
  }
}

exports.get_input = function (question) {
  return new Promise((resolve) => {
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    });
    rl.question(question, (choice) => {
      rl.close();
      resolve(choice);
    })
  });
}
