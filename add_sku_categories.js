const rp = require('request-promise');
const async = require('async');
const _ = require('lodash');
const Promise = require("bluebird")
const fs =  Promise.promisifyAll(require("fs"));
const login = require('./lib/login')
const io = require('./lib/io')


const categoryChoices = [
  'Accessory',
  'Activation',
  'Insurance',
  'Top Tier Device',
  'Upgrade',
]

// Use this to set a set of SKUs to be Top Tier for a customer.
// Need to set the cookie to a valid session cookie (can get it from dev tools after logged in off sku API requests)

// To add a mapping:
// PUT https://insight.envysion.com/api/v3/skus/types
// body: {"code":"ACVZRB000803","types":{"defined":["Top Tier Device"]}}







function addSkuCategory(sku_code, category, cookie, callback) {
  var j = rp.jar();
  var url = "https://insight.envysion.com/api/v3/skus/types";
  j.setCookie(cookie, url);

  body = {
    code : sku_code,
    types : { defined: [category] }
  };

  var options = {
    url: url,
    jar: j,
    body: JSON.stringify(body),
    resolveWithFullResponse: true
  };

  rp.put(options)
    .then(function(response){
      console.log(sku_code, " Status = ", response.statusCode);
      callback(null);
    })
    .catch(function(err, response){
      console.log("*******************FAILED ON", sku_code, " ",err.statusCode, err);
      // eat the error for now
      callback(null);
    });

}

function addItems(sku_codes, category, cookie) {
  console.log("There are ", sku_codes.length, " Items to add");

  async.eachLimit(sku_codes, 8, function(sku, callback) {
    console.log("Adding ", sku, ">>>>>>>>>>");
    addSkuCategory(sku, category, cookie, callback);
  }, function(err){
    if (err) { console.log("FAILED With Error", err) }
  });
}

function loadSkus() {
  return fs.readFileAsync("skus.txt").then((data) =>{
    let arr = data.toString().split('\n')
    return arr.filter(function(entry) { return entry.trim() != ''; });
  })
}

function addData(domain, password) {
  let idx = null
  let cookie = null
  login.loginForToken(domain, password)
    .then((val) => {
      cookie = "jwt_token="+val.token
      io.print_category_choices(categoryChoices)
      return io.get_input('Please enter the category number to map to SKUs :>  ')
    })
    .then((number) => {
      idx = (parseInt(number) - 1)
      return loadSkus()
    })
    .then((skuArr) => {
      let category = categoryChoices[idx]
      console.log("Mapping", skuArr.length, "SKUS to category", category)
      addItems(skuArr, category, cookie)
    })
}

function main() {
  let domain = null
  let password = null

  let p1 = io.get_input('Please enter the domain (example pgwireless.com) :>  ')
  p1.then(function(domain){
    let p2 = io.get_input('Please enter envysion@ password :>  ')
    p2.then(function(password){
      addData(domain, password)
    })
  })
}

main()
