# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Very simple tool to let you bulk map SKUs via API calls.

### How do I get set up? ###

Node 8+  
`npm install`  
Fill out the skus.txt file with the skus you want to map one on each line.  
`node ./add_sku_categories.js`  
When you runt the program, it will prompt you for the domain, login info, and the category you are mapping.  


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

John Noble
jnoble@envysion.com
john@springboxventures.com

