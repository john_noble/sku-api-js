var rp = require('request-promise');
var async = require('async');
var _ = require('lodash');
const login = require('./lib/login');
const io = require('./lib/io');
const Promise = require("bluebird");
const fs =  Promise.promisifyAll(require("fs"));

const url = 'https://insight.envysion.com/api/v3/skus';


// Use this to clear select mapped SKUs for a customer.
// Need to set the cookie to a valid session cookie (can get it from dev tools after logged in off sku API requests)

// To add a mapping:
// PUT https://insight.envysion.com/api/v3/skus/types
// body: {"code":"ACVZRB000803","types":{"defined":["Upgrades"]}}



const choices = [
  'Clear All Currently Mapped SKUs',
  'Clear SKUs listed in clearskus.txt file',
]


// clearItems(items, cookie);

function clearSku(sku, cookie, callback) {
  const j = rp.jar();
  j.setCookie(cookie, url);

  body = {
    code : sku,
    types : { defined: [] }
  };

  var options = {
    url: url+ "/types",
    jar: j,
    body: JSON.stringify(body),
    resolveWithFullResponse: true
  };

  rp.put(options)
    .then(function(response){
      console.log("OK Status = ", response.statusCode);
      callback(null);
    })
    .catch(function(err){
      console.log("EXITING, ERROR! ", err);
      callback(err);
      exit(1);
    });

}

function clearItems(mapped_skus, cookie) {
  console.log("There are ", mapped_skus.length, " Items to clear");

  async.eachLimit(mapped_skus, 1, function(e, callback) {
    console.log("Removing ", e);
    clearSku(e, cookie, callback);
  }, function(err){ if (err) { console.log("ERROR!", err); } });
}

// load the currently mapped skus from the API
function loadAllMappedSkus(cookie) {
  const j = rp.jar();
  j.setCookie(cookie, url);

  var options = {
    url: url,
    jar: j,
    json: true
  };

  return rp(options)
  .then(function(parsedBody) {
    mapped_skus = _.filter(parsedBody, function(e) { return e["types"]["defined"].length > 0; })
    return(_.map(mapped_skus, 'code'));
  })
  .catch(function(err){
    console.log("EXITING, ERROR FETCHING SKUS! ", err);
    exit(1);
  });
}

function loadSkusFromFile() {
  return fs.readFileAsync("clearskus.txt").then((data) =>{
    let arr = data.toString().split('\n')
    return arr.filter(function(entry) { return entry.trim() != ''; });
  })
}

function loadSkus(choiceIdx, cookie) {
  if (choiceIdx == 0) {
      return loadAllMappedSkus(cookie);
  }
  return loadSkusFromFile();
}

function clearData(domain, password) {
  let idx = null
  let cookie = null
  login.loginForToken(domain, password)
    .then((val) => {
      cookie = "jwt_token="+val.token
      io.print_category_choices(choices)
      return io.get_input('Please enter the method to clear:>  ')
    })
    .then((number) => {
      idx = (parseInt(number) - 1)
      return loadSkus(idx, cookie)
    })
    .then((skuArr) => {
      console.log("Clearing", skuArr.length, "SKUS from all categories")
      clearItems(skuArr, cookie)
    })
}

function main() {
  let domain = null
  let password = null

  let p1 = io.get_input('Please enter the domain (example pgwireless.com) :>  ')
  p1.then(function(domain){
    let p2 = io.get_input('Please enter envysion@ password :>  ')
    p2.then(function(password){
      clearData(domain, password)
    })
  })
}

main()
